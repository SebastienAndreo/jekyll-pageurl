module Jekyll
    class PageUrl  < Liquid::Tag
        def initialize(tag_name, text, tokens)
            super
            values = text.split(',')

            @link = nil
            @url = nil

            if(values.length != 1)
                raise ArgumentError.new <<-eos
                wrong format "#{@text}" in tag 'page_url'.
                eos
            end

            @url = values[0].strip
   
            urlvalues = @url.split('#')
                 
            if urlvalues.length == 1
                @link = urlvalues[0].strip
            else
                @link = urlvalues[0].strip
                @anchor = urlvalues[1].strip
            end
        end

        def render(context)
            site = context.registers[:site]

            site.each_site_file do |p|

                baseurl = site.config["baseurl"]
                url = site.config["url"]

                if @link == p.relative_path
                    if @anchor != nil
                        full_url=  url + baseurl +  p.url + "/#" + @anchor
                    else
                        full_url=  url + baseurl +  p.url
                    end

                    return "#{ full_url }"
                end
            end
            raise ArgumentError.new <<-eos
                Could not find page "#{@link}" in tag 'page_url'.
                Make sure the post exists and the name and date is correct.
                eos
        end
    end
end

Liquid::Template.register_tag('page_url', Jekyll::PageUrl)