# Jekyll Pageurl

This Jekyll pluging provides a tag `page_url` that takes a reference to an internal page and generate url of that file in the static website.
this plugin is similar to the `page_link` plugin without the `<a>` beacon

## Examples

### Automatic href inference

```
{% page_url _howto/gettingstarted.md %}
```

In that case the the plugin will calculate from the referenced page `_howto/gettingstarted.md `, the corresponding url.


### specifiying an anchor

in the page `_pages/example-page.md` we have defined an anchor hparagraph

```
{% page_url _pages/example-page.md#hparagraph %}
```

```
/jekyll-theme-siemens-newton/howto/gettingstarted/#hparagraph
```

## Installation

Add this line to your Gemfile:

```ruby
group :jekyll_plugins do
  gem "jekyll-pageurl"
end
```

And then execute:

    $ bundle install

Alternatively install the gem yourself as:

    $ gem install jekyll-pageurl

and put this in your ``_config.yml``

```yaml
plugins:
    - jekyll-pageurl
```

## Release notes

### Version 1.0.2

- Only baseurl was considered now if the url is defined it will take it first

### Version 1.0.0

- Initial version, facilitate the creation of intern site links